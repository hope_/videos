import React, { Component } from "react";
import "../styles/SearchBar.css";
class Searchbar extends Component {
  state = { term: "" };
  onFormSubmit = (event) => {
    event.preventDefault();
    this.props.onTermSubmit(this.state.term);
  };
  render() {
    return (
      <div className="searchbar-container">
        <form onSubmit={this.onFormSubmit}>
          <div className="field">
            <label htmlFor="video-search">Videos Search</label>
            <input
              id="video-search"
              type="text"
              value={this.state.term}
              onChange={(e) => {
                this.setState({ term: e.target.value });
              }}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default Searchbar;
