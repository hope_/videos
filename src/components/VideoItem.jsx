import React from "react";
import "../styles/Video.css";

const VideoItem = ({ video, onVideoSelect }) => {
  return (
    <div onClick={() => onVideoSelect(video)} className="video-container">
      <img
        src={video.snippet.thumbnails.medium.url}
        alt={video.snippet.description}
      />
      <div className="title">{video.snippet.title}</div>
    </div>
  );
};
export default VideoItem;
