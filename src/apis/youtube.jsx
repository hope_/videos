import axios from "axios";
const KEY = "AIzaSyAwaFhFSQR_o464oLD7B6EtzW713KEt-Ww";
export default axios.create({
  baseURL: " https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    maxResults: 10,
    type: "video",
    key: KEY,
  },
});
